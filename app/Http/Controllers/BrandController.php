<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use PDF;
class BrandController extends Controller
{
    //
    public function index()
    {
        $brands = Brand::latest()->get();
        // dd($categories);
        return view('backend.brands.index', [
            'brands' => $brands,
        ]);
    }

    public function create()
    {
        return view('backend.brands.create');
    }

    public function store(Request $request)
    {

        try {
            $request->validate([
                'name' => 'required',
                'title' => 'required|min:5',

                // 'title' => ['required', 'min:3', Rule::unique('categories', 'title')],
                'description' => ['required', 'min:20'],
            ]);
            // $imageName = request()->file('image')->store('storage/app/public/images');
            // dd($imageName);
            Brand::create([
                'name'=> $request->name,

                'title' => $request->title,
                'description' => $request->description,
                'images'=>$request->images,
            ]);

            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('brands.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }

    // // public function show(Category $id)
    public function show(Brand $brands)
    {
        // dd($brands);
        // $category = Category::where('id', $id)->firstOrFail();
        // $category = Category::find($id);
        return view('backend.brands.show', [
            'brands' => $brands
        ]);
    }

    public function edit(Brand $brands)
    {
        return view('backend.brands.edit', [
            'brands' => $brands
        ]);
    }

    public function update(Request $request, Brand $brands)
    {
        try {
            // $request->validate([
            //     // 'title' => 'required|min:3|max:50|unique:categories,title,'.$category->id,
            //     'title' => ['required', 'min:3', 
            //         Rule::unique('categories', 'title')->ignore($category->id)
            //     ],
            //     'description' => ['required', 'min:10'],
            // ]);

            $brands->update([
                'name'=>$request->name,
                'title' => $request->title,
                'description' => $request->description,
            ]);

            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('brands.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }

    public function destroy(Brand $brands)
    {
        try {
            $brands->delete();
            return redirect()->route('brands.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }

    public function trash()
    {
        $brands = Brand::onlyTrashed()->latest()->get();
        return view('backend.brands.trash', [
            'brands' => $brands,
        ]);
 
    }
    public function restore($id)
    {
        $brands = Brand::onlyTrashed()->findOrFail($id);
        $brands->restore();
        return redirect()->route('brands.trash')->withMessage('Successfully Restore!');
    }
    public function delete($id)
    {
        $category = Brand::onlyTrashed()->findOrFail($id);
        $category->forceDelete();
        return redirect()->route('brands.trash')->withMessage('Successfully Deleted Permanently!');
    }
    public function downloadPdf()
    {
       
        $brands = Brand::all();
        $pdf = PDF::loadView('backend.brands.pdf', compact('brands'));
        return $pdf->download('brands.pdf');
    }



}
