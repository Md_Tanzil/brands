<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Brands
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Brands </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Brands</li>
             

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Brands <a class="btn btn-sm btn-info" href="{{ route('brands.create') }}">Add New</a>
            <a class="btn btn-sm btn-info" href="{{ route('brands.trash') }}">Trush</a>
            <a class="btn btn-sm btn-primary" href="{{ route('brands.pdf') }}">PDF Download</a>
        </div>
        <div class="card-body">

            @if (session('message'))
            <div class="alert alert-success">
                <span class="close" data-dismiss="alert">&times;</span>
                <strong>{{ session('message') }}.</strong>
            </div>
            @endif

            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>Sl#</th>
                        <th>Name</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $sl=0 @endphp
                    @foreach ($brands as $brands)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $brands->name}}</td>

                        <td>{{ $brands->title }}</td>
                        <td>{{ $brands->description }}</td>
                        <td>
                            <a class="btn btn-info btn-sm" href="{{ route('brands.show', ['brands' => $brands->id]) }}" >Show</a>

                            <a class="btn btn-warning btn-sm" href="{{ route('brands.edit', ['brands' => $brands->id]) }}" >Edit</a>

                            <form style="display:inline" action="{{ route('brands.destroy', ['brands' => $brands->id]) }}" method="post">
                                @csrf
                                @method('delete')
                                
                                <button onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                            </form>

                            {{-- <!-- <a href="{{ route('categories.destroy', ['category' => $category->id]) }}" >Delete</a> --> --}}


                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

</x-backend.layouts.master>