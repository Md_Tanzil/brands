<table id="datatablesSimple" border="1">
    <thead>
        <tr>
            <th>Sl#</th>
            <th>Title</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
    @php $sl=0 @endphp
        @foreach ($brands as $brands)
        <tr>
            <td>{{ ++$sl }}</td>
            <td>{{ $brands->title }}</td>
            <td>{{ $brands->description }}</td>
            
        </tr>
        @endforeach

    </tbody>
</table>